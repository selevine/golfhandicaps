package entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Course {
	
	@Id
    @GeneratedValue
	private long courseId;
	@NotNull
	@Column(unique=true)
	private String name;
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name="courseid")
	private List<Layout> layouts;
	private String streetAddress;
	@NotNull
	private String city;
	@NotNull
	private String state;
	private String country;
	private String postalCode;
	
	public Course() {
	}

	public long getCourseId() {
		return courseId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public List<Layout> getLayouts() {
		return layouts;
	}

	public void setLayouts(List<Layout> layouts) {
		this.layouts = layouts;
	}

	@Override
	public String toString() {
		return "Course [courseId=" + courseId + ", name=" + name + ", layouts=" + layouts + ", streetAddress="
				+ streetAddress + ", city=" + city + ", state=" + state + ", country=" + country + ", postalCode="
				+ postalCode + "]";
	}

}
