package entities;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Round {
	
	@Id
	@GeneratedValue
	private long roundId;
	@NotNull
	private long layoutId;
	@NotNull
	private long playerId;
	@NotNull
	private Calendar datePlayed;
	@NotNull
	private float handicap;
	
	public Round() {
	}

	public long getRoundId() {
		return roundId;
	}

	public void setRoundId(long roundId) {
		this.roundId = roundId;
	}

	public long getLayoutId() {
		return layoutId;
	}

	public void setLayoutId(long layoutId) {
		this.layoutId = layoutId;
	}

	public long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public Calendar getDatePlayed() {
		return datePlayed;
	}

	public void setDatePlayed(Calendar datePlayed) {
		this.datePlayed = datePlayed;
	}

	public float getHandicap() {
		return handicap;
	}

	public void setHandicap(float handicap) {
		this.handicap = handicap;
	}

}
