package entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Player {
	
	@Id
    @GeneratedValue
	private long playerId;
	@NotNull
	private String firstName;
	@NotNull
	private String lastName;
	private String homeClub;
	private float handicapIndex;
	
	public Player() {
	}

	public long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getHomeClub() {
		return homeClub;
	}

	public void setHomeClub(String homeClub) {
		this.homeClub = homeClub;
	}

	public float getHandicapIndex() {
		return handicapIndex;
	}

	public void setHandicapIndex(float handicapIndex) {
		this.handicapIndex = handicapIndex;
	}

	
}	