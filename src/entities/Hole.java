package entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Hole {
	
	@Id
	@GeneratedValue
	private long holeId;
	@NotNull
	private long layoutId;
	@NotNull
	private int holeNumber;
	@NotNull
	private int yardage;
	@NotNull
	private int par;
	private int handicap;
	
	public Hole() {
	}

	public long getHoleId() {
		return holeId;
	}

	public long getLayoutId() {
		return layoutId;
	}

	public void setLayoutId(long layoutId) {
		this.layoutId = layoutId;
	}

	public int getHoleNumber() {
		return holeNumber;
	}

	public void setHoleNumber(int holeNumber) {
		this.holeNumber = holeNumber;
	}

	public int getYardage() {
		return yardage;
	}

	public void setYardage(int yardage) {
		this.yardage = yardage;
	}

	public int getPar() {
		return par;
	}

	public void setPar(int par) {
		this.par = par;
	}

	public int getHandicap() {
		return handicap;
	}

	public void setHandicap(int handicap) {
		this.handicap = handicap;
	}
	
}
