package entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Layout {
	
	@Id
	@GeneratedValue
	private long layoutId;
	@NotNull
	private long courseId;
	@NotNull
	private Tee tee;
	@NotNull
	private Gender gender;
	@NotNull
	private int slope;
	@NotNull
	private float rating;
	
	public Layout() {
	}
	
	public long getLayoutId() {
		return layoutId;
	}
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public Tee getTee() {
		return tee;
	}
	public void setTee(Tee tee) {
		this.tee = tee;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public int getSlope() {
		return slope;
	}
	public void setSlope(int slope) {
		this.slope = slope;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}

}
