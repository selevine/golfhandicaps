package entities;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

public class Score {
	
	@Id
	@GeneratedValue
	private long scoreId;
	@NotNull
	private byte strokes;
	private byte equitableStrokes;
	private byte putts;
	private byte penalty;
	
	public long getScoreId() {
		return scoreId;
	}
	public void setScoreId(long scoreId) {
		this.scoreId = scoreId;
	}
	public byte getStrokes() {
		return strokes;
	}
	public void setStrokes(byte strokes) {
		this.strokes = strokes;
	}
	public byte getEquitableStrokes() {
		return equitableStrokes;
	}
	public void setEquitableStrokes(byte equitableStrokes) {
		this.equitableStrokes = equitableStrokes;
	}
	public byte getPutts() {
		return putts;
	}
	public void setPutts(byte putts) {
		this.putts = putts;
	}
	public byte getPenalty() {
		return penalty;
	}
	public void setPenalty(byte penalty) {
		this.penalty = penalty;
	}

	
}
