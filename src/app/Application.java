package app;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.Course;
import entities.Gender;
import entities.Hole;
import entities.Layout;
import entities.Tee;

public class Application {
	
	private static EntityManagerFactory emFactory;
	private static EntityManager em;
	private static List<Course> courses;
	private static List<Layout> layouts;
	private static List<Hole> holes;
	
	public static void main(String[] args) {
		
		emFactory = Persistence.createEntityManagerFactory("GOLF_SCH01");
		em = emFactory.createEntityManager();
		em.getTransaction().begin();
//		generateCourses();
//		for(Course course: courses) {em.persist(course);};
//		generateLayoutsforUMGC();
////		generateHolesforUMGCChampionship();
////		generateHolesforUMGCBack();
//		for(Layout layout: layouts) {em.persist(layout);};
//		for(Hole hole: holes) {em.persist(hole);};
//		em.getTransaction().commit();
//		courses = getAllCourses();
		printLayoutsforUMGC();
		em.close();
		emFactory.close();
//		for(Course course: courses) {System.out.println(course);};
	}
	
	
	public static void generateCourses() {
		if(courses == null) {courses = new ArrayList<Course>();}
		courses.add(courseBuilder("University of Michigan Golf Course","500 E. Stadium Blvd","Ann Arbor","MI","US","48104"));
		courses.add(courseBuilder("Radrick Farms","4875 Geddes Rd","Ann Arbor","MI","US","48105"));
		courses.add(courseBuilder("Leslie Park Golf Course","2120 Traver Rd","Ann Arbor","MI","US","48105"));
	}
	
	public static void generateLayoutsforUMGC() {
		if(layouts == null) {layouts = new ArrayList<Layout>();}
		Course umgc = getCourseByName("University of Michigan Golf Course").get(0);
		long umgcId = umgc.getCourseId();
		System.out.println("CourseId: " + umgcId + " " + umgc.getName());
		layouts.add(layoutBuilder(umgcId, Tee.Championship, Gender.Men, 135, 72.8f));
		layouts.add(layoutBuilder(umgcId, Tee.Back, Gender.Men, 131, 71.0f));
		layouts.add(layoutBuilder(umgcId, Tee.Standard, Gender.Men, 128, 69.4f));
		layouts.add(layoutBuilder(umgcId, Tee.Forward, Gender.Men, 119, 66.2f));
		layouts.add(layoutBuilder(umgcId, Tee.Championship, Gender.Women, 147, 79.8f));
		layouts.add(layoutBuilder(umgcId, Tee.Back, Gender.Women, 145, 78.1f));
		layouts.add(layoutBuilder(umgcId, Tee.Standard, Gender.Women, 141, 75.6f));
		layouts.add(layoutBuilder(umgcId, Tee.Forward, Gender.Women, 128, 71.3f));
		System.out.println("Number of layouts:" + layouts.size());
	}
	
	public static void printLayoutsforUMGC() {
		Course umgc = getCourseByName("University of Michigan Golf Course").get(0);
		System.out.println("CourseId: " + umgc.getCourseId());
		List<Layout> layouts = getLayoutsByCourseId(umgc.getCourseId());
		for(Layout layout: layouts) {System.out.println("LayoutId: " + layout.getLayoutId() + " "
				+ " " + umgc.getName() + " " + layout.getTee() + " " + layout.getGender());
		}
	}
	
	public static void generateHolesforUMGCChampionship() {
		Course umgc = getCourseByName("University of Michigan Golf Course").get(0);
		System.out.println("CourseId: " + umgc.getCourseId());
		List<Layout> layouts = getLayoutsByCourseId(umgc.getCourseId());
		for(Layout layout: layouts) {System.out.println("LayoutId: " + layout.getLayoutId() + " "
				+ " " + umgc.getName() + " " + layout.getTee() + " " + layout.getGender());
		}
		if(holes == null) {holes = new ArrayList<Hole>();}
		holes.add(holeBuilder(101, 1, 546, 5, 11)); 
		holes.add(holeBuilder(101, 2, 423, 4, 1)); 
		holes.add(holeBuilder(101, 3, 537, 5, 5)); 
		holes.add(holeBuilder(101, 4, 417, 4, 3)); 
		holes.add(holeBuilder(101, 5, 225, 3, 17)); 
		holes.add(holeBuilder(101, 6, 308, 4, 13)); 
		holes.add(holeBuilder(101, 7, 378, 4, 9)); 
		holes.add(holeBuilder(101, 8, 159, 3, 15)); 
		holes.add(holeBuilder(101, 9, 411, 4, 7)); 
		holes.add(holeBuilder(101, 10, 417, 4, 6)); 
		holes.add(holeBuilder(101, 11, 533, 5, 4)); 
		holes.add(holeBuilder(101, 12, 247, 3, 12)); 
		holes.add(holeBuilder(101, 13, 320, 4, 16)); 
		holes.add(holeBuilder(101, 14, 149, 3, 18)); 
		holes.add(holeBuilder(101, 15, 389, 4, 14)); 
		holes.add(holeBuilder(101, 16, 417, 4, 8)); 
		holes.add(holeBuilder(101, 17, 399, 4, 10)); 
		holes.add(holeBuilder(101, 18, 455, 4, 2)); 
		System.out.println("Number of holes:" + holes.size());
	}
		
		public static void generateHolesforUMGCBack() {
			if(holes == null) {holes = new ArrayList<Hole>();}
			holes.add(holeBuilder(102, 1, 486, 5, 11)); 
			holes.add(holeBuilder(102, 2, 412, 4, 1)); 
			holes.add(holeBuilder(102, 3, 523, 5, 5)); 
			holes.add(holeBuilder(102, 4, 403, 4, 3)); 
			holes.add(holeBuilder(102, 5, 183, 3, 17)); 
			holes.add(holeBuilder(102, 6, 295, 4, 13)); 
			holes.add(holeBuilder(102, 7, 354, 4, 9)); 
			holes.add(holeBuilder(102, 8, 149, 3, 15)); 
			holes.add(holeBuilder(102, 9, 397, 4, 7)); 
			holes.add(holeBuilder(102, 10, 372, 4, 6)); 
			holes.add(holeBuilder(102, 11, 507, 5, 4)); 
			holes.add(holeBuilder(102, 12, 238, 3, 12)); 
			holes.add(holeBuilder(102, 13, 308, 4, 16)); 
			holes.add(holeBuilder(102, 14, 143, 3, 18)); 
			holes.add(holeBuilder(102, 15, 379, 4, 14)); 
			holes.add(holeBuilder(102, 16, 395, 4, 8)); 
			holes.add(holeBuilder(102, 17, 368, 4, 10)); 
			holes.add(holeBuilder(102, 18, 436, 4, 2)); 
			System.out.println("Number of holes:" + holes.size());
	}
		
		public static void generateHolesforUMGCStandard() {
			if(holes == null) {holes = new ArrayList<Hole>();}
			holes.add(holeBuilder(102, 1, 442, 5, 3)); 
			holes.add(holeBuilder(102, 2, 355, 4, 5)); 
			holes.add(holeBuilder(102, 3, 504, 5, 1)); 
			holes.add(holeBuilder(102, 4, 394, 4, 13)); //par is 4/5
			holes.add(holeBuilder(102, 5, 156, 3, 17)); 
			holes.add(holeBuilder(102, 6, 273, 4, 11)); 
			holes.add(holeBuilder(102, 7, 346, 4, 9)); 
			holes.add(holeBuilder(102, 8, 140, 3, 15)); 
			holes.add(holeBuilder(102, 9, 391, 4, 7)); 
			holes.add(holeBuilder(102, 10, 356, 4, 6)); 
			holes.add(holeBuilder(102, 11, 488, 5, 2)); 
			holes.add(holeBuilder(102, 12, 223, 3, 18)); //par is 3/4
			holes.add(holeBuilder(102, 13, 293, 4, 14)); 
			holes.add(holeBuilder(102, 14, 127, 3, 16)); 
			holes.add(holeBuilder(102, 15, 370, 4, 4)); 
			holes.add(holeBuilder(102, 16, 369, 4, 10)); //par is 4/5
			holes.add(holeBuilder(102, 17, 353, 4, 12)); 
			holes.add(holeBuilder(102, 18, 419, 5, 8)); 
			System.out.println("Number of holes:" + holes.size());
		}
	
	public static Course courseBuilder(String name, String street, String city, String state, String country, String postalCode) {
		Course course = new Course();
		course.setName(name);
		course.setStreetAddress(street);
		course.setCity(city);
		course.setState(state);
		course.setCountry(country);
		course.setPostalCode(postalCode);
		return course;
	}
	
	public static Layout layoutBuilder(long courseId, Tee tee, Gender gender, int slope, float rating) {
		Layout layout = new Layout();
		layout.setCourseId(courseId);
		layout.setTee(tee);
		layout.setGender(gender);
		layout.setSlope(slope);
		layout.setRating(rating);
		return layout;
	}
	
	public static Hole holeBuilder(long layoutId, int number, int yardage, int par, int handicap) {
		Hole hole = new Hole();
		hole.setLayoutId(layoutId);
		hole.setHoleNumber(number);
		hole.setYardage(yardage);
		hole.setPar(par);
		hole.setHandicap(handicap);
		return hole;
	}

	public static List<Course> getAllCourses() {
		return (List<Course>) em.createQuery("SELECT c FROM Course AS c", Course.class).getResultList();
	}
	
	public static List<Layout> getAllLayouts() {
		return (List<Layout>) em.createQuery("SELECT l FROM Layout AS l", Layout.class).getResultList();
	}
	
	public static List<Course> getCourseByName(String name) {
		return (List<Course>) em.createQuery("SELECT c FROM Course c WHERE c.name like :courseName")
				.setParameter("courseName", name)
				.getResultList();
	}
	
	public static List<Layout> getLayoutsByCourseId(long courseId) {
		return (List<Layout>) em.createQuery("SELECT l FROM Layout l WHERE l.courseId like :cid")
				.setParameter("cid", courseId)
				.getResultList();
	}
	
}
